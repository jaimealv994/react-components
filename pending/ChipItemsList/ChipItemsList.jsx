/**
 * Created by Jaime Noel Alvarez Luna on 06/09/2017.
 */

import React from 'react';
import PropTypes from 'prop-types';
import ChipItem from '../ChipItem';

const ChipItemsList = ({value, titleGenerator, onRequestDelete}) => {
  return (
    <div style={{display: 'flex', flexWrap: 'wrap'}}>
      {value.map(item => {
        const title = titleGenerator(item);
        return <ChipItem handleDelete={onRequestDelete} item={item} key={title} label={title} />;
      })}
    </div>
  );
};

ChipItemsList.propTypes = {
  onRequestDelete: PropTypes.func.isRequired,
  titleGenerator: PropTypes.func.isRequired,
  value: PropTypes.array.isRequired,
};

export default ChipItemsList;
