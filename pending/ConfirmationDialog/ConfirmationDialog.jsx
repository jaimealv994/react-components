/**
 * Created by Jaime Noel Alvarez Luna on 11/15/2017.
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import Dialog from 'material-ui/Dialog';
import DialogTitle from 'material-ui/Dialog/DialogTitle';
import DialogActions from 'material-ui/Dialog/DialogActions';
import Slide from 'material-ui/transitions/Slide';

class ConfirmationDialog extends Component {
  closeDialog = () => {
    const {notificationActions} = this.props;
    notificationActions.closeConfirmationDialog();
  };

  handleAgreeAction = () => {
    const {action} = this.props;
    action();
    this.closeDialog();
  };

  render() {
    const {title, visibility} = this.props;
    return (
      <Dialog onClose={this.closeDialog} open={visibility} title={title} transition={Slide}>
        <DialogTitle>{title}</DialogTitle>
        <DialogActions>
          // eslint-disable-next-line react/jsx-handler-names
          <Button color="primary" onClick={this.closeDialog}>
            Cancelar
          </Button>
          <Button color="primary" onClick={this.handleAgreeAction}>
            Aceptar
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

ConfirmationDialog.propTypes = {
  action: PropTypes.func,
  notificationActions: PropTypes.object,
  title: PropTypes.any,
  visibility: PropTypes.bool,
};

export default ConfirmationDialog;
