export default {
  palette: {
    primary: {
      light: '#2770AA',
      main: '#01579b',
      dark: '#002c6d',
      contrastText: '#fff',
    },
    secondary: {
      light: '#78909C',
      main: '#607D8B',
      dark: '#263238',
      contrastText: '#fff',
    },
    // error: will us the default color
  },
};
