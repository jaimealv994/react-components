import React from 'react';
import PropTypes from 'prop-types';
import Chip from 'material-ui/Chip';

/** Chip item component */
const ChipItem = ({label, handleDelete, item}) => {
  const handleRequestDelete = () => {
    handleDelete(item);
  };

  return (
    <Chip label={label} onDelete={handleDelete ? handleRequestDelete : null} style={{margin: 4}} />
  );
};

ChipItem.propTypes = {
  /** Handler for deleting */
  handleDelete: PropTypes.func,
  /** Item to handle */
  item: PropTypes.any,
  /** Text to be displayed */
  label: PropTypes.any,
};

export default ChipItem;
