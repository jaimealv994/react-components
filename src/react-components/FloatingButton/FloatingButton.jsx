import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import styles from './styles';
import AddIcon from '@material-ui/icons/Add';

const FloatingButton = ({children = <AddIcon />, color = 'primary', onClick, ...props}) => {
  return (
    <Button color={color} onClick={onClick} style={styles.floatingButton} variant="fab" {...props}>
      {children}
    </Button>
  );
};

FloatingButton.propTypes = {
  children: PropTypes.node,
  color: PropTypes.string,
  onClick: PropTypes.func,
};

export default FloatingButton;
