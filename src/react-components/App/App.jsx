import React from 'react';
import PropTypes from 'prop-types';
import Navigation from './Navigation';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';

const App = ({theme, ...props}) => {
  if (theme) {
    const theme = createMuiTheme(theme);
    return (
      <MuiThemeProvider theme={theme}>
        <Navigation {...props} />
      </MuiThemeProvider>
    );
  }

  return <Navigation {...props} />;
};

App.propTypes = {
  children: PropTypes.node.isRequired,
  menus: PropTypes.node.isRequired,
  theme: PropTypes.oneOf([PropTypes.object, PropTypes.func]),
  title: PropTypes.string.isRequired,
};

export default App;
