import React, {Component} from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import classNames from 'classnames';
import styles from './styles';
import Toolbar from '@material-ui/core/Toolbar';
import withStyles from '@material-ui/core/styles/withStyles';

class Navigation extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    classes: PropTypes.object.isRequired,
    logo: PropTypes.string,
    menus: PropTypes.node.isRequired,
    openMenuDelay: PropTypes.number,
    theme: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
  };

  static defaultProps = {
    openMenuDelay: 3000,
    logo: null,
  };

  state = {
    open: true,
    disableMenu: true,
  };

  componentDidMount() {
    const {openMenuDelay} = this.props;

    setTimeout(() => {
      this.setState(() => ({disableMenu: false}));
      this.handleDrawerClose();
    }, openMenuDelay);
  }

  handleDrawerClose = () => this.setState(() => ({open: false}));
  handleDrawerOpen = () => this.setState(() => ({open: true}));

  render() {
    const {classes, theme, menus, children, logo, title} = this.props;
    const {open, disableMenu} = this.state;

    return (
      <div className={classes.root}>
        <AppBar
          className={classNames(classes.appBar, open && classes.appBarShift)}
          position="absolute"
        >
          <Toolbar className={classes.titleBar} disableGutters={!open}>
            <div style={{display: 'flex'}}>
              <IconButton
                aria-label="open drawer"
                className={classNames(classes.menuButton, open && classes.hide)}
                color="inherit"
                onClick={this.handleDrawerOpen}
              >
                <MenuIcon />
              </IconButton>
              {logo && (
                <img
                  alt="logo"
                  height={48}
                  src={logo}
                  style={{display: open ? 'none' : 'initial'}}
                />
              )}
            </div>
            <Typography color="inherit" noWrap variant="title">
              {title}
            </Typography>
            <div />
          </Toolbar>
        </AppBar>
        <Drawer
          classes={{
            paper: classNames(classes.drawerPaper, !open && classes.drawerPaperClose),
          }}
          open={open}
          variant="permanent"
        >
          <div className={classes.toolbar}>
            {logo && (
              <img alt="logo" height={48} src={logo} style={{display: open ? 'initial' : 'none'}} />
            )}
            <IconButton disabled={disableMenu} onClick={this.handleDrawerClose}>
              {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
            </IconButton>
          </div>
          <Divider />

          {menus}
        </Drawer>
        <main className={classNames(classes.content, open && classes.contentDrawerOpen)}>
          <div className={classes.toolbar} />
          {children}
        </main>
      </div>
    );
  }
}

export default withStyles(styles, {withTheme: true})(Navigation);
