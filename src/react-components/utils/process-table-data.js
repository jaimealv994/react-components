import createCachedSelector from 're-reselect';
import valueResolver from 'object-resolve-path';
import {getProps, processCacheName} from 'easy-reselect';

const getValue = ({formatter, dateFormat, key}, data, keyToGet = key) => {
  const value = valueResolver(data, keyToGet);

  if (formatter) return formatter(value, data);
  else if (dateFormat && value)
    return dateFormat(value, typeof dateFormat === 'boolean' ? 'DD/MM/YYYY' : dateFormat);
  else return value;
};

const processData = createCachedSelector(
  getProps('data', 'columnData', 'orderBy', 'order', 'searchText'),
  (data, columnData, orderBy, order, searchText) => {
    return sortData({
      data: filterData({searchText, columnData, data}),
      orderBy,
      order,
      columnData,
    });
  }
)(processCacheName('data', 'columnData', 'orderBy', 'order', 'searchText'));

const filterData = createCachedSelector(
  getProps('searchText', 'columnData', 'data'),
  (searchText, columnsData, data) => {
    if (searchText === '' || !searchText) return data;

    const exp = new RegExp(searchText, 'gi');

    return data.filter(row => {
      return columnsData.find(columnData => {
        return columnData.searchable && exp.test(getValue(columnData, row));
      });
    });
  }
)(processCacheName('searchText', 'columnData', 'data'));

const sortData = createCachedSelector(
  getProps('data', 'orderBy', 'order', 'columnData'),
  (data, orderBy, order, columnsData) => {
    if (!orderBy) return data;

    const columnData = columnsData.find(columnData => columnData.key === orderBy);

    const inverter = order === 'desc' ? 1 : -1;
    return [...data].sort((a, b) => {
      const value1 = getValue(columnData, a, orderBy),
        value2 = getValue(columnData, b, orderBy);

      return (value1 < value2 ? -1 : value1 > value2 ? 1 : 0) * inverter;
    });
  }
)(processCacheName('data', 'orderBy', 'order', 'columnData'));

export default processData;
