import filtering from './filtering';
import processTableData from './process-table-data';

export {filtering, processTableData};
