export default function(data, criteria, filter) {
  if (!filter) return data;

  return data.filter(dataValue => {
    criteria.forEach(criteriaValue => {
      if (dataValue[criteriaValue].toLowerCase().indexOf(filter) !== -1) return true;
    });

    return false;
  });
}
