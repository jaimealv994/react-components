import React from 'react';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CloseIcon from '@material-ui/icons/Close';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';

const DialogTitle = ({fullScreen, classes, onClose, title, rightElement, takeColor}) => {
  if (!fullScreen) {
    return (
      <MuiDialogTitle className={takeColor ? classes.titleContainer : undefined}>
        <Typography className={takeColor ? classes.title : undefined} variant="title">
          title
        </Typography>
      </MuiDialogTitle>
    );
  } else {
    return (
      <AppBar style={styles.appBar}>
        <Toolbar>
          <IconButton aria-label="Close" color="inherit" onClick={onClose}>
            <CloseIcon />
          </IconButton>
          <Typography color="inherit" style={styles.flex} variant="title">
            {title}
          </Typography>
          {rightElement}
        </Toolbar>
      </AppBar>
    );
  }
};

DialogTitle.defaultProps = {
  fullScreen: false,
  takeColor: false,
};

DialogTitle.propTypes = {
  classes: PropTypes.object,
  fullScreen: PropTypes.bool,
  handleClose: PropTypes.func,
  rightElement: PropTypes.any,
  takeColor: PropTypes.bool,
  title: PropTypes.string.isRequired,
};

export default withStyles(styles)(DialogTitle);
