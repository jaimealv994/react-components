export default theme => {
  console.log(theme);
  return {
    appBar: {
      position: 'relative',
    },
    flex: {
      flex: 1,
    },

    titleContainer: {
      backgroundColor: theme.palette.primary.main,
    },

    title: {
      color: theme.palette.primary.contrastText,
    },
  };
};
