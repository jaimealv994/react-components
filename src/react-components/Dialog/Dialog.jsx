import React from 'react';
import PropTypes from 'prop-types';
import MuiDialog from '@material-ui/core/Dialog';
import DialogTitle from './DialogTitle';

const Dialog = ({children, fullScreen, title, onClose, rightElement, coloredTitle, ...props}) => {
  return (
    <MuiDialog fullScreen={fullScreen} onClose={onClose} {...props}>
      <DialogTitle
        fullScreen={fullScreen}
        handleClose={onClose}
        rightElement={rightElement}
        takeColor={coloredTitle}
        title={title}
      />
      {children}
    </MuiDialog>
  );
};

Dialog.defaultProps = {
  maxWidth: 'md',
};

Dialog.propTypes = {
  children: PropTypes.node,
  fullScreen: PropTypes.bool,
  maxWidth: PropTypes.oneOf(['xs', 'sm', 'md', false]),
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  rightElement: PropTypes.any,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  transition: PropTypes.func,
};

export default Dialog;
