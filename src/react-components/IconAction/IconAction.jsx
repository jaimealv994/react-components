import React from 'react';
import PropTypes from 'prop-types';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';

const IconAction = ({icon, title, onClick, color = 'inherit'}) => {
  return (
    <Tooltip title={title}>
      <IconButton color={color} onClick={onClick}>
        {icon}
      </IconButton>
    </Tooltip>
  );
};

IconAction.propTypes = {
  color: PropTypes.string,
  icon: PropTypes.node.isRequired,
  onClick: PropTypes.func,
  title: PropTypes.string,
};

export default IconAction;
