import React, {Component} from 'react';
import PropTypes from 'prop-types';
import MuiTableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Checkbox from '@material-ui/core/Checkbox';
import resolver from 'object-resolve-path';

export default class TableRow extends Component {
  static propTypes = {
    columnData: PropTypes.array.isRequired,
    dataRow: PropTypes.object.isRequired,
    displayCheckbox: PropTypes.bool.isRequired,
    isSelected: PropTypes.bool.isRequired,
    onSelectionChange: PropTypes.func.isRequired,
  };

  handleClick = e => {
    const {displayCheckbox, isSelected, onSelectionChange, dataRow} = this.props;
    if (displayCheckbox) onSelectionChange(e, dataRow, !isSelected);
  };

  render() {
    const {isSelected, displayCheckbox, columnData, dataRow} = this.props;
    return (
      <MuiTableRow
        aria-checked={isSelected}
        hover
        onClick={this.handleClick}
        role="checkbox"
        selected={isSelected}
        tabIndex={-1}
      >
        {displayCheckbox && (
          <TableCell padding="checkbox">
            <Checkbox checked={isSelected} />
          </TableCell>
        )}

        {columnData.map(({formatter, key, numeric = false, padding = 'default'}) => {
          const data = !formatter
            ? resolver(dataRow, key)
            : formatter(resolver(dataRow, key), dataRow);

          return (
            <TableCell key={key} numeric={numeric} padding={padding} variant="body">
              {data}
            </TableCell>
          );
        })}
      </MuiTableRow>
    );
  }
}
