import React, {Component} from 'react';
import PropTypes from 'prop-types';
import MuiTableBody from '@material-ui/core/TableBody';
import TableRow from './TableRow';
import resolver from 'object-resolve-path';

class TableBody extends Component {
  isSelected = data => {
    const {selected, idRef} = this.props;

    return (
      selected.find(dataRow => resolver(dataRow, idRef) === resolver(data, idRef)) !== undefined
    );
  };

  render() {
    const {data, columnData, displayCheckbox, idRef, onSelectionChange} = this.props;

    return (
      <MuiTableBody>
        {data.map(dataRow => {
          const isSelected = this.isSelected(dataRow);

          return (
            <TableRow
              columnData={columnData}
              dataRow={dataRow}
              displayCheckbox={displayCheckbox}
              isSelected={isSelected}
              key={resolver(dataRow, idRef)}
              onSelectionChange={onSelectionChange}
            />
          );
        })}
      </MuiTableBody>
    );
  }
}

TableBody.propTypes = {
  columnData: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      numeric: PropTypes.bool,
      padding: PropTypes.oneOf(['default', 'checkbox', 'dense', 'none', undefined]),
      formatter: PropTypes.func,
    })
  ).isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  displayCheckbox: PropTypes.bool,
  idRef: PropTypes.string.isRequired,
  onSelectionChange: PropTypes.func,
  selected: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default TableBody;
