import React from 'react';
import PropTypes from 'prop-types';
import MuiTableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableHeadColumn from './TableHeadColumn';
import TableCell from '@material-ui/core/TableCell';

const TableHead = ({columnData, orderBy, directionSort, onRequestSort, displayCheckbox}) => {
  return (
    <MuiTableHead>
      <TableRow>
        {displayCheckbox && <TableCell padding="checkbox" style={{width: 48}} />}
        {columnData.map(({key, ...props}) => {
          return (
            <TableHeadColumn
              active={key === orderBy}
              direction={directionSort}
              key={key}
              keyValue={key}
              onRequestSort={onRequestSort}
              {...props}
            />
          );
        })}
      </TableRow>
    </MuiTableHead>
  );
};

TableHead.propTypes = {
  columnData: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      formatter: PropTypes.func,
    })
  ),
  directionSort: PropTypes.string,
  displayCheckbox: PropTypes.bool,
  onRequestSort: PropTypes.func,
  orderBy: PropTypes.string,
};

export default TableHead;
