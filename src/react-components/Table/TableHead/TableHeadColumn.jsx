import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TableCell from '@material-ui/core/TableCell';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Tooltip from '@material-ui/core/Tooltip';

export default class TableHeadColumn extends Component {
  static propTypes = {
    active: PropTypes.bool,
    direction: PropTypes.oneOf(['asc', 'desc', undefined]),
    keyValue: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    numeric: PropTypes.bool,
    onRequestSort: PropTypes.func,
    padding: PropTypes.oneOf(['checkbox', 'dense', 'none', undefined]),
    sortable: PropTypes.bool,
    style: PropTypes.object,
  };

  handleClick = () => {
    this.props.onRequestSort(this.props.keyValue);
  };

  render() {
    const {active, label, numeric, padding, style, sortable, direction} = this.props;
    return (
      <TableCell numeric={numeric} padding={padding} style={style} variant="head">
        {sortable ? (
          <Tooltip
            enterDelay={300}
            placement={numeric ? 'bottom-end' : 'bottom-start'}
            title="Ordenar"
          >
            <TableSortLabel active={active} direction={direction} onClick={this.handleClick}>
              {label}
            </TableSortLabel>
          </Tooltip>
        ) : (
          label
        )}
      </TableCell>
    );
  }
}
