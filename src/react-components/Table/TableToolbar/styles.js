import {lighten} from '@material-ui/core/styles/colorManipulator';

export default theme => ({
  appBar: {
    boxShadow: 'initial',
  },
  root: {
    paddingRight: theme.spacing.unit,
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  actions: {
    color: theme.palette.text.secondary,
  },
  title: {
    flex: 1,
  },
  searchForm: {
    alignItems: 'center',
    color: 'inherit',
    marginRight: 8,
    flexDirection: 'row',
  },
});
