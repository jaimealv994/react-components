import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import SearchIcon from '@material-ui/icons/Search';
import Input from '@material-ui/core/Input';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';

const TableToolbar = ({
  title,
  searchable,
  searchText,
  customActions,
  onSearchInputChange,
  classes,
  numSelected,
}) => {
  return (
    <AppBar className={classes.appBar} color="inherit" position="static">
      <Toolbar
        className={classNames(classes.root, {
          [classes.highlight]: numSelected > 0,
        })}
      >
        <Typography
          className={classes.title}
          color={numSelected === 0 ? undefined : 'inherit'}
          variant={numSelected === 0 ? 'title' : 'subheading'}
        >
          {title}
        </Typography>
        <div className={classes.actions}>{customActions}</div>
        {!customActions &&
          searchable && (
            <FormControl className={classes.searchForm}>
              <Typography color="inherit" variant="title">
                <SearchIcon />
              </Typography>
              <Input onChange={onSearchInputChange} placeholder="Búsqueda" value={searchText} />
            </FormControl>
          )}
      </Toolbar>
    </AppBar>
  );
};

TableToolbar.propTypes = {
  classes: PropTypes.object,
  customActions: PropTypes.any,
  numSelected: PropTypes.number.isRequired,
  onSearchInputChange: PropTypes.func.isRequired,
  searchText: PropTypes.string.isRequired,
  searchable: PropTypes.bool,
  title: PropTypes.string,
};

export default withStyles(styles)(TableToolbar);
