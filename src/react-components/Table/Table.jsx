import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import TableToolbar from './TableToolbar';
import MuiTable from '@material-ui/core/Table';
import TablePagination from '@material-ui/core/TablePagination';
import TableHead from './TableHead';
import TableBody from './TableBody';
import TableFooter from '@material-ui/core/TableFooter';
import TableRow from '@material-ui/core/TableRow';
import {processTableData} from '../utils';
import styles from './styles';
import resolver from 'object-resolve-path';

export default class Table extends Component {
  static propTypes = {
    actionsGenerator: PropTypes.func,
    columnData: PropTypes.arrayOf(
      PropTypes.shape({
        key: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
        selectedElementTitleRef: PropTypes.string,
        selectable: PropTypes.bool,
      })
    ).isRequired,
    data: PropTypes.array.isRequired,
    elevation: PropTypes.number,
    idRef: PropTypes.string,
    multiSelect: PropTypes.bool,
    onSelectionChange: PropTypes.func,
    rowsPerPage: PropTypes.number,
    selectedElementTitleRef: PropTypes.string,
    title: PropTypes.string,
  };

  static defaultProps = {
    idRef: 'id',
    multiSelect: false,
    rowsPerPage: 5,
  };

  state = {
    selectable: false,
    selected: [],
    page: 0,
    rowsPerPage: 5,
    data: [],
    columnData: null,
    searchable: false,
    searchText: '',
  };

  static getDerivedStateFromProps({columnData, data, rowsPerPage, actionsGenerator}, prevState) {
    const state = {};
    let changed = false;

    if (columnData !== prevState.columnData) {
      state.columnData = columnData;
      if (columnData.find(({selectable}) => selectable)) state.selectable = true;
      if (columnData.find(({searchable}) => searchable)) {
        state.searchable = true;
      }
      changed = true;
    }

    if (actionsGenerator && !prevState.hasActionsGenerator) {
      state.selectable = true;
      state.hasActionsGenerator = true;
      changed = true;
    }

    if (rowsPerPage !== prevState.rowsPerPage) {
      state.rowsPerPage = rowsPerPage;
      changed = true;
    }

    if (JSON.stringify(data) !== JSON.stringify(prevState.data)) {
      state.data = [...data];
      state.selected = [];
      changed = true;
    }

    if (changed) return state;
    else return null;
  }

  handleRequestSort = orderBy => {
    let order = 'desc';
    if (this.state.orderBy === orderBy && this.state.order === 'desc') order = 'asc';

    this.setState({order, orderBy});
  };

  handleSelectionChange = (event, value, actionType) => {
    const {selected} = this.state;
    const {multiSelect, idRef, onSelectionChange} = this.props;
    let newSelected;

    if (actionType) {
      if (multiSelect)
        newSelected = selected.concat(value).sort((next, prev) => {
          const prevValue = resolver(prev, idRef),
            nextValue = resolver(next, idRef);
          if (prevValue < nextValue) return 1;
          else if (prevValue > nextValue) return -1;
          return 0;
        });
      else newSelected = [value];
    } else newSelected = selected.filter(item => item !== value);

    this.setState(() => ({selected: newSelected}));
    if (onSelectionChange) onSelectionChange(newSelected);
  };
  handleChangePage = () => {};
  handleChangeRowsPerPage = event => this.setState({rowsPerPage: event.target.value});

  handleSearchChange = event => {
    this.setState({searchText: event.target.value});
  };

  render() {
    const {elevation, title, actionsGenerator, idRef, selectedElementTitleRef} = this.props;
    const {
      selectable,
      rowsPerPage,
      data,
      columnData,
      searchable,
      selected,
      searchText,
      page,
      order,
      orderBy,
    } = this.state;

    const processedData = processTableData({
      data,
      columnData,
      orderBy,
      order,
      searchText,
    });
    const numSelected = selected.length;

    return (
      <Paper elevation={elevation}>
        <TableToolbar
          customActions={numSelected !== 0 && actionsGenerator ? actionsGenerator(selected) : null}
          numSelected={selected.length}
          onSearchInputChange={this.handleSearchChange}
          searchText={searchText}
          searchable={searchable}
          title={
            numSelected === 0
              ? title
              : numSelected === 1 && selectedElementTitleRef
                ? resolver(selected[0], selectedElementTitleRef)
                : `${selected.length} seleccionados`
          }
        />

        <div style={styles.tableRoot}>
          <MuiTable>
            <TableHead
              columnData={columnData}
              directionSort={order}
              displayCheckbox={selectable}
              onRequestSort={this.handleRequestSort}
              orderBy={orderBy}
            />

            <TableBody
              columnData={columnData}
              data={processedData.slice(page * rowsPerPage, rowsPerPage * (page + 1))}
              displayCheckbox={selectable}
              idRef={idRef}
              onSelectionChange={this.handleSelectionChange}
              selected={selected}
            />

            <TableFooter>
              <TableRow>
                <TablePagination
                  count={processedData.length}
                  labelRowsPerPage="Filas por Página: "
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                  page={page}
                  rowsPerPage={rowsPerPage}
                />
              </TableRow>
            </TableFooter>
          </MuiTable>
        </div>
      </Paper>
    );
  }
}
