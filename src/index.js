import React from 'react';
import ReactDOM from 'react-dom';
import Test from './demo';

ReactDOM.render(<Test />, document.getElementById('root'));
