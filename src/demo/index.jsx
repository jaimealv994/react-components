import React from 'react';
import Dialog from '../react-components/Dialog';
import DialogContent from '@material-ui/core/DialogContent';

const Demo = () => {
  return (
    <Dialog fullWidth open title="Prueba">
      <DialogContent>
        <span>text</span>
      </DialogContent>
    </Dialog>
  );
};

export default Demo;
